import { View, Pressable, StyleSheet } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

export default function CircleButtons({ onPress }) {
    return (
        <View style={styles.CircleButtonContainer}>
            <Pressable style={styles.CircleButton} onPress={onPress}>
                <MaterialIcons name="add" size={30} color={"#1c1c1e"} />
            </Pressable>
        </View>
    );
}

const styles = StyleSheet.create({
    CircleButtonContainer: {
        width: 84,
        height: 84,
        marginHorizontal: 60,
        borderWidth: 4,
        borderColor: "#ffd33d",
        borderRadius: 42,
        padding: 3,
    },
    CircleButton: {
        flex: 1,
        alignItems: "center",
        borderRadius: 42,
        backgroundColor: "#fff",
        justifyContent: "center", // Añadido para centrar el icono verticalmente
    },
});