

import { StyleSheet } from "react-native";
import { Pressable, Text, View } from "react-native";

import { FontAwesome } from "@expo/vector-icons";

export default function Button({ label, theme, onPress }) {

    if (theme === 'primary') {

        return (
            <View style={[styles.buttonContainer, { borderWidth: 4, borderColor : '#cccccc', borderRadius : 12 }]}>
                <Pressable
                    style={[styles.Button, { backgroundColor : '#ffffff' }]}
                    onPress={onPress}>

                    <FontAwesome
                        name="picture-o"
                        size={16}
                        color='#25292e'
                        style={styles.buttonIcons}
                    />

                    <Text style={[styles.buttonLabel, { color : '#25292e' }]}>
                        {label}
                    </Text>
                </Pressable>
            </View>
        );






    };

    return (
        <View style={styles.buttonContainer}>
            <Pressable style={styles.Button}>
                <Text style={styles.buttonLabel}>{label}</Text>
            </Pressable>
        </View>
    );
}

const styles = StyleSheet.create({

    buttonContainer : {
        widdth : 320,
        height : 60,
        padding : 4,
        marginHorizontal: 12,
        alignItem : 'center',
        justifyContent : 'center'
    },

    Button : {
        borderRadius : 8,
        paddingStart : 40,
        paddingEnd : 40,
        width : '100%',
        height : '100%',
        alignItems :  'center',
        alignContent : 'center',
        flexDirection : 'row',
    },

    buttonLabel : {
        color : '#fff',
        fontSize: 14,
    },

    buttonIcon : {
        paddingLeft : 12,
    },



});

// -------------------------------------------------------------------------- //