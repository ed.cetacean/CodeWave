import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import ApiBtc from './components/ApiBtc';
import Post from './components/Post';

export default function App() {
  return (
    <View style={styles.container}>
      <Post />
      <ApiBtc />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFDEDE',
    alignItems: 'center',
    justifyContent: 'center',
  },
});