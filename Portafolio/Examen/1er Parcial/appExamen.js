
// ----------------------------- NFL: ED RUBIO  ----------------------------- //

const axios = require('axios');
const readline = require('readline');
const examenURL = 'http://jsonplaceholder.typicode.com/todos';


// Obtiene la lista completa de datos del JSON.
async function listaPendientes() {

    try {
        const response = await axios.get(examenURL);
        return response.data;
    } catch (error) {
        console.error('- Ha ocurrido un error al obtener la lista de pendientes: ', error.message);
        return [];
    }
}

// -------------------------------------------------------------------------- //

// Lista de todos los pendientes (sólo IDs).
async function listaPendientesIDs() {
    const pendientes = await listaPendientes();
    const ids = pendientes.map((pendiente) => pendiente.id);

    console.clear();
    console.log('\n- Lista de pendientes (únicamente IDs): ', ids);
}

// Lista de todos los pendientes (IDs y Titles).
async function listaPendientesIDsTitles() {
    const pendientes = await listaPendientes();
    const datos = pendientes.map((pendiente) => ({ id: pendiente.id, title: pendiente.title }));

    console.clear();
    console.log('\n- Lista de pendientes (IDs y Titles): ', datos);
}

// Lista de todos los pendientes sin resolver (IDs y Titles)
async function listaPendientesSRIDsTitles() {
    const pendientes = await listaPendientes();
    const pendientesSinResolver = pendientes.filter((pendiente) => !pendiente.completed);
    const datos = pendientesSinResolver.map((pendiente) => ({ id: pendiente.id, title: pendiente.title }));

    console.clear();
    console.log('\n- Lista de pendientes sin resolver (IDs y Titles): ', datos);
}

// Lista de todos los pendientes resueltos (IDs y Titles).
async function listaPendientesYRIDsTitles() {
    const pendientes = await listaPendientes();
    const pendientesResueltos = pendientes.filter((pendiente) => pendiente.completed);
    const datos = pendientesResueltos.map((pendiente) => ({ id: pendiente.id, title: pendiente.title }));

    console.clear();
    console.log('\n- Lista de pendientes resueltos (IDs y Titles): ', datos);
}

// Lista de todos los pendientes (IDs y UserIDs).
async function listaPendientesIDsUserIDs() {
    const pendientes = await listaPendientes();
    const datos = pendientes.map((pendiente) => ({ id: pendiente.id, userId: pendiente.userId }));

    console.clear();
    console.log('\n- Lista de pendientes (IDs y UserIDs): ', datos);
}

// Lista de todos los pendientes resueltos (IDs y UserIDs).
async function listaPendientesYRIDsUserIDs() {
    const pendientes = await listaPendientes();
    const pendientesResueltos = pendientes.filter((pendiente) => pendiente.completed);
    const datos = pendientesResueltos.map((pendiente) => ({ id: pendiente.id, userId: pendiente.userId }));

    console.clear();
    console.log('\n- Lista de pendientes resueltos (IDs y UserIDs): ', datos);
}

// Lista de todos los pendientes sin resolver (IDs y UserIDs).
async function listaPendientesSRIDsUserIDs() {
    const pendientes = await listaPendientes();
    const pendientesSinResolver = pendientes.filter((pendiente) => !pendiente.completed);
    const datos = pendientesSinResolver.map((pendiente) => ({ id: pendiente.id, userId: pendiente.userId }));

    console.clear();
    console.log('\n- Lista de pendientes sin resolver (IDs y UserIDs): ', datos);
}

// ------------------------------- APLICACIÓN ------------------------------- //

// Menü de selección. :)
async function ejecutarMenu() {

    const Read = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    console.log('\n- ¡Sea usted bienvenido a la pizarra de pendientes!');

    while(true) {
        console.log('\n1) Lista de todos los pendientes (sólo IDs)');
        console.log('2) Lista de todos los pendientes (IDs y Titles)');
        console.log('3) Lista de todos los pendientes sin resolver (IDs y Titles)');
        console.log('4) Lista de todos los pendientes resueltos (IDs y Titles)');
        console.log('5) Lista de todos los pendientes (IDs y UserIDs)');
        console.log('6) Lista de todos los pendientes resueltos (IDs y UserIDs)');
        console.log('7) Lista de todos los pendientes sin resolver (IDs y UserIDs)');
        console.log('8) Salir');

        const opcionConsulta = await new Promise((resolve) => {
            Read.question('\n- Ingrese el número de la opción que desea consultar: ', (respuesta) => { resolve(parseInt(respuesta)); });
        });

        if (!isNaN(opcionConsulta) && opcionConsulta > 0 && opcionConsulta < 9) {

            switch (opcionConsulta) {

                case 1:
                    await listaPendientesIDs(); break;
                case 2:
                    await listaPendientesIDsTitles(); break;
                case 3:
                    await listaPendientesSRIDsTitles(); break;
                case 4:
                    await listaPendientesYRIDsTitles(); break;
                case 5:
                    await listaPendientesIDsUserIDs(); break;
                case 6:
                    await listaPendientesYRIDsUserIDs(); break;
                case 7:
                    await listaPendientesSRIDsUserIDs(); break;
                case 8:
                    console.log('\n- Nos vemos pronto...'); Read.close(); process.exit(0); break;
                default:
                    console.log('\n- ¡Uh, parece que ha ocurrido un errorcillo tímido! :(');
                    await consultarOpcion(); break;

            } let usuarioContinuar = '';

            while (usuarioContinuar !== 'S' && usuarioContinuar !== 'N') {

                usuarioContinuar = await new Promise((resolve) => {
                    Read.question('\n- ¿Desea realizar alguna otra consulta (S/N)? ', (respuesta) => {
                        resolve(respuesta.toUpperCase());
                    });
                });
            }

            if (usuarioContinuar === 'N') {
                console.log('\n- Nos vemos pronto...');
                Read.close(); process.exit(0);
            } else { console.clear(); }

        } else { console.clear(); console.log('\n- Ha introducido un valor no válido. ¡Por favor, intente nuevamente (1-8)!'); }

    }

}

console.clear(); ejecutarMenu(); // Muestra el menú de selección al iniciar la aplicación.

// -------------------------------------------------------------------------- //