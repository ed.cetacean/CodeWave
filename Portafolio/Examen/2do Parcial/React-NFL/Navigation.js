
// ────────────────────────────────────────────────────────────────────────── //

import 'react-native-gesture-handler';

import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator, DrawerItemList } from '@react-navigation/drawer';
import { View, ScrollView, SafeAreaView, Image, Text, StyleSheet } from 'react-native';

// ────────────────────────────────────────────────────────────────────────── //

import First from './screens/First';
import Second from './screens/Second';
import Third from './screens/Third';
import Fourth from './screens/Fourth';
import Fifth from './screens/Fifth';
import Sixth from './screens/Sixth';
import Seventh from './screens/Seventh';


// Paleta de colores:
const mainColor = '#1C1C1E';
const subColor = '#E3DDD3';

const Drawer = createDrawerNavigator();
const Logo = require('./assets/Logo.png')

// ────────────────────────────────────────────────────────────────────────── //

const Navigation = () => {

    return (

        <NavigationContainer>
            <Drawer.Navigator

                drawerContent = {(props) => {

                    return (
                        <SafeAreaView style={ styles.mainContainer }>

                            <View style={styles.appInfo}>
                                <Image source={ Logo } style={styles.logoPicture} />
                                <Text style={styles.nameText}>NFL: Ed Rubio</Text>
                            </View>

                            <ScrollView style={styles.mainContainer} showsVerticalScrollIndicator={false} >
                                <DrawerItemList {...props} />
                            </ScrollView>

                        </SafeAreaView>
                    );
                }}

                // ────────────────────────────────────────────────────────── //

                screenOptions = {{

                    // ENCABEZADO: ------------------------------------------ //

                    headerShown: true,
                    headerTintColor: subColor,
                    headerStyle: { borderBottomWidth: 0, backgroundColor: mainColor },

                    // PANEL: ----------------------------------------------- //

                    drawerActiveBackgroundColor: subColor,
                    drawerActiveTintColor: mainColor,
                    drawerInactiveTintColor: subColor,
                    drawerStyle: styles.drawerStyle,
                    overlayColor: "rgba(28, 28, 30, .6)",
                    drawerLabelStyle: styles.drawerLabelStyle, }}>


                <Drawer.Screen name='PendientesID' component={ First } options={{
                    title : "Pendientes (IDs)" }} />

                <Drawer.Screen name='PendientesIDandTitles' component={ Second } options={{
                    title : "Pendientes (IDs & Títulos)" }} />

                <Drawer.Screen name='SinResolverIDandTitles' component={ Third } options={{
                    title : "Pendientes sin resolver (IDs & Títulos)" }} />

                <Drawer.Screen name='ResueltosIDandTitles' component={ Fourth } options={{
                    title : "Pendientes resueltos (IDs & Títulos)" }} />

                <Drawer.Screen name='PendientesIDandUserID' component={ Fifth } options={{
                    title : "Pendientes (IDs & IDs de Usuario)" }} />

                <Drawer.Screen name='ResueltosIDandUserID' component={ Sixth } options={{
                    title : "Pendientes resueltos (IDs & IDs de Usuario)" }} />

                <Drawer.Screen name='SinResolverIDandUserID' component={ Seventh } options={{
                    title : "Pendientes sin resolver (IDs & IDs de Usuario)" }} />

            </Drawer.Navigator>
        </NavigationContainer>
    );
};

export default Navigation;

// ────────────────────────────────────────────────────────────────────────── //

const styles = StyleSheet.create({

    mainContainer: {
        flex: 1,
    },

    appInfo: {
        paddingTop: 80,
        paddingBottom: 40,
        backgroundColor: mainColor,
    },

    logoPicture: {
        height: 180,
        width: '100%',
        marginBottom: 40,
        alignSelf: 'center',
    },

    nameText: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
        color: subColor,
    },

    drawerLabelStyle: {
        fontSize: 14,
    },

    drawerStyle: {
        backgroundColor: mainColor,
    },

});

// ────────────────────────────────────────────────────────────────────────── //