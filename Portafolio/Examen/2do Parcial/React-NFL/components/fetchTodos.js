
// ────────────────────────────────────────────────────────────────────────── //

import axios from 'axios';

// ────────────────────────────────────────────────────────────────────────── //

const BASE_URL = 'http://jsonplaceholder.typicode.com';

export async function fetchTodos() {

    try {
        const response = await axios.get(`${BASE_URL}/todos`);
        return response.data;

    } catch (error) {
        console.error('Ha ocurrido un error: ', error);
        throw error;
    }
}

// ────────────────────────────────────────────────────────────────────────── //

export function filterPendingTodos(todos) {
    return todos.filter(todo => !todo.completed);
};

export function mapTodosToIDsAndUserIDs(todos) {
    return todos.map(todo => ({ id: todo.id, userId: todo.userId }));
};

export function filterCompletedTodos(todos) {
    return todos.filter(todo => todo.completed);
};

export function filterTodosByUserID(todos, userID) {
    return todos.filter(todo => todo.userId === userID);
};

export function mapTodosToIDs(todos) {
    return todos.map(todo => todo.id);
};

export function mapTodosToIDsAndTitles(todos) {
    return todos.map(todo => ({ id: todo.id, title: todo.title }));
};

export function filterCompletedTodosAndUserIDs(todos) {
    return todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));
};

// ────────────────────────────────────────────────────────────────────────── //