import React, { useState, useEffect } from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import { DataTable } from 'react-native-paper';
import { fetchTodos, mapTodosToIDsAndTitles } from '../components/fetchTodos';

export default function Second() {
  const [todosIDsAndTitles, setTodosIDsAndTitles] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const todos = await fetchTodos();
        const idsAndTitles = mapTodosToIDsAndTitles(todos);
        setTodosIDsAndTitles(idsAndTitles);
      } catch (error) {
        console.error('Error fetching todos IDs and titles:', error);
      }
    }

    fetchData();
  }, []);

  return (
    <View style={styles.mainContainer}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.tableContainer}>

          <DataTable style={styles.dataTable}>

            <DataTable.Header style={styles.dataHeader}>
              <DataTable.Title>ID</DataTable.Title>
              <DataTable.Title>Título</DataTable.Title>
            </DataTable.Header>

            {todosIDsAndTitles.map(todo => (
              <DataTable.Row key={todo.id}>
                <DataTable.Cell>{todo.id}</DataTable.Cell>
                <DataTable.Cell>{todo.title}</DataTable.Cell>
              </DataTable.Row>
            ))}
          </DataTable>
        </View>
      </ScrollView>
    </View>
  );
}


const styles = StyleSheet.create({

  mainContainer: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 120,
  },

  scrollContainer: {
    flexGrow: 1,
  },

  tableContainer: {
    padding: 20,
  },

  dataTable: {
    borderWidth: 1,
    borderColor: '#ddd',
  },

  dataHeader: {
    backgroundColor: '#ddd',
  },

});