
// ────────────────────────────────────────────────────────────────────────── //

import React, { useState, useEffect } from 'react';
import { View, ScrollView, ImageBackground, StyleSheet } from 'react-native';
import { DataTable } from 'react-native-paper';
import { fetchTodos, mapTodosToIDs } from '../components/fetchTodos';

// ────────────────────────────────────────────────────────────────────────── //

export default function First() {
  const [todosIDs, setTodosIDs] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const todos = await fetchTodos();
        const ids = mapTodosToIDs(todos);
        setTodosIDs(ids);
      } catch (error) {
        console.error('Ha ocurrido un error al consultar las IDs: ', error);
      }
    }

    fetchData();
  }, []);

  // ──────────────────────────────────────────────────────────────────────── //

  return (

    <View style={styles.mainContainer}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.tableContainer}>

          <DataTable style={styles.dataTable}>

            <DataTable.Header style={styles.dataHeader}>
              <DataTable.Title>ID</DataTable.Title>
            </DataTable.Header>

            {todosIDs.map(id => (
              <DataTable.Row key={id}>
                <DataTable.Cell>{id}</DataTable.Cell>
              </DataTable.Row>
            ))}

          </DataTable>

        </View>
      </ScrollView>
    </View>

  );
}

// ────────────────────────────────────────────────────────────────────────── //

const styles = StyleSheet.create({

  mainContainer: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 120,
  },

  scrollContainer: {
    flexGrow: 1,
  },

  tableContainer: {
    padding: 20,
  },

  dataTable: {
    borderWidth: 1,
    borderColor: '#ddd',
  },

  dataHeader: {
    backgroundColor: '#ddd',
  },

});

// ────────────────────────────────────────────────────────────────────────── //