import React, { useState, useEffect } from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import { DataTable } from 'react-native-paper';
import { fetchTodos, mapTodosToIDsAndUserIDs } from '../components/fetchTodos';

export default function Fifth() {
  const [todosIDsAndUserIDs, setTodosIDsAndUserIDs] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const todos = await fetchTodos();
        const idsAndUserIDs = mapTodosToIDsAndUserIDs(todos);
        setTodosIDsAndUserIDs(idsAndUserIDs);
      } catch (error) {
        console.error('Error fetching todos IDs and userIDs:', error);
      }
    }

    fetchData();
  }, []);

  return (
    <View style={styles.mainContainer}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.tableContainer}>

          <DataTable style={styles.dataTable}>

            <DataTable.Header style={styles.dataHeader}>
              <DataTable.Title>ID</DataTable.Title>
              <DataTable.Title>ID de Usuario</DataTable.Title>
            </DataTable.Header>

            {todosIDsAndUserIDs.map(todo => (
              <DataTable.Row key={todo.id}>
                <DataTable.Cell>{todo.id}</DataTable.Cell>
                <DataTable.Cell>{todo.userId}</DataTable.Cell>
              </DataTable.Row>
            ))}
          </DataTable>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({

  mainContainer: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 120,
  },

  scrollContainer: {
    flexGrow: 1,
  },

  tableContainer: {
    padding: 20,
  },

  dataTable: {
    borderWidth: 1,
    borderColor: '#ddd',
  },

  dataHeader: {
    backgroundColor: '#ddd',
  },

});